var path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
    entry: {
        actionLink: './src/jsx/ActionLink.jsx',
        clock: './src/jsx/Clock.jsx',
        greeting: './src/jsx/Greeting.jsx',
        hello: './src/jsx/hello.jsx',
        loggingButton: './src/jsx/LoggingButton.jsx',
        loginControl: './src/jsx/LoginControl.jsx',
        toggle: './src/jsx/Toggle.jsx'
    },
    devtool: 'inline-source-map',
    devServer: {
        contentBase: './dist'
    },
    plugins: [
        new CleanWebpackPlugin(['dist']),
        new HtmlWebpackPlugin({
            title: 'Hi React JS'
        })
    ],
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'dist')
    }
};