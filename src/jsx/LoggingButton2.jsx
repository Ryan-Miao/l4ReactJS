class LoggingButton extends React.Component {

    handleClick() {
        console.log("this is ", this);
    };

    render() {
        return (
            //This syntax ensures `this` is bind within handleClick
            //不建议这样做，有性能问题，因为每次都会创建新的callback，推荐在构造函数初始化的时候binding
            <button onClick={(e) => this.handleClick(e)}>
                Click me, auto bind this by lambda
            </button>
        );
    }
}

ReactDOM.render(
    <LoggingButton />,
    document.getElementById('loggingButton')
);
